import frappe


def update_item_end_of_life(doc, *args):
    # can be made more efficient
    print doc.items
    for item in doc.items:
        frappe.db.set_value('Item', item.item_code, 'end_of_life', item.expiry_date)