import frappe


@frappe.whitelist()
def get_item_expiry_date(item_code):
    return frappe.db.get_value('Item', item_code, 'end_of_life')