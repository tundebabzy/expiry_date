import frappe


def execute():
    doc = frappe.get_doc('Custom Script', 'Purchase Invoice-Client')
    doc.dt = 'Purchase Invoice'
    script = doc.get('script', '')
    new_script = """
frappe.ui.form.on('Purchase Invoice Item', {
	item_code: function(frm, cdt, cdn) {
		const d = locals[cdt][cdn];
		frappe.call({
			method: 'expiry_date.expiry_date.utils.get_item_expiry_date',
			args: {'item_code': d.item_code},
			callback: function(r) {
				if (r.message) {
					frappe.model.set_value(cdt, cdn, 'expiry_date', r.message);
				}
			}
		});
	}
});
"""
    doc.script = '{0}\n{1}'.format(script, new_script)

    doc.save()